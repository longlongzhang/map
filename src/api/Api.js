import axios from 'axios'

axios.defaults.baseURL = 'http://10.11.18.129:8090/ruting/';
// axios.defaults.baseURL = 'http://192.168.1.106:8090/ruting/';


/**
 * get 请求
 */
export function get(url) {
  return new Promise((resolve, reject) => {
    axios.get(url)
      .then(response => {
        resolve(response.data);
      }, err => {
        reject(err);
      })
      .catch((error) => {
        reject(error)
      })
  })
}

/**
 * post 请求
 */
export function post(url, params) {
  return new Promise((resolve, reject) => {
    axios.post(url, params)
      .then(response => {
        resolve(response.data);
      }, err => {
        reject(err);
      })
      .catch((error) => {
        reject(error)
      })
  })
}

// export function formatParams(params) {
//   return '?page=' + params.page + '&pageSize=' + params.pageSize;
// }


export default {

  /**
   * company 位置信息列表
   */
  getCompanyList() {
    return get('map/company/list')
  },
}
